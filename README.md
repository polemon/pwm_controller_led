# Pwm Controller Led

PWM controller for 12V LED light fixtures.

***

![PWM controller](./pwm_controller_led.png)*PWM Controller*

## Description

Simple PWM controller for 12V LED light fixtures.
Control range is from complete zero to 100% by change of the dutycycle.
